var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const axios = require('axios');
const fs = require('fs');

// Config
const secrets = require('./config/secrets.js');

// Routing
var indexRouter = require('./routes/index');
var voteRouter = require('./routes/vote');

var app = express();

var Worker = require('./worker.js');
var ExchangeListener = require('./exchangeListener.js');
var ExchangeLogger = require('./ExchangeLogger.js');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/vote', voteRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

// RabbitMQ handlers


// Start a listener that will append from the queue
// to a log file
var exchangeLogger1 = ExchangeLogger('voteLogs');

// Stop the file logger
setTimeout(() => {
    exchangeLogger1.connection.close();
}, 5000);

// Start it up again
setTimeout(() => {
    exchangeLogger1 = ExchangeLogger('voteLogs');
}, 10000);

// Spawn workers as need be
let workers = [];
// Start up a listener that will log the queued messages
// to the console
workers.push(ExchangeListener('voteLogs'));
// How many messages each worker should be able to handle
let workload = 10;

let manageWorkers = function(data) {
    let capacity = workers.length * workload;
    let jobs = data.messages;
    let difference = jobs - capacity;
    let overflow = Math.floor(Math.abs(difference)/workload);

    fs.appendFile('workers.log', workers.length +
        '\n' + jobs +
        '\n' + JSON.stringify(data), function(err) {
        if(err) {
            console.log(err);
        }
    });

    console.log(difference, overflow);
    // If too many jobs, add workers
    if(difference > workload) {
        for(let i = 0; i < overflow; i++) {
            console.log('Spinning up new worker');
            workers.push(ExchangeListener('voteLogs'));
        }
    } else {
        for(let i = 0; i < overflow; i++) {
            if(workers.length == 1) {
                // Don't remove the last worker
                return;
            }
            console.log('Shutting worker down');
            let unused = workers.shift();
            unused.connection.close();
            delete unused;
        }
    }
}

var CronJob = require('cron').CronJob;
const job = new CronJob('*/3 * * * * *', () => {
    axios.get('http://localhost:15672/api/queues', {
        auth: {
            username: secrets.rabbitmq.user,
            password: secrets.rabbitmq.password
        }
    })
    .then((res) => {
        let queueData = res.data.find(x => x.name == 'exchangeListener');
        return queueData;
    })
    .catch((err) => {
        console.log(err);
    })
    .then((data) => {
        manageWorkers(data);
    });
});
job.start();

// Can use API to get further information on queued messages
// e.g. use url http://localhost:15672/api/overview?lengths_age=60&lengths_incr=5&msg_rates_age=60&msg_rates_incr=5
// and then deduct "samples" statistics from each other to see amount of messages
// delivered per time
