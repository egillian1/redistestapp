var amqp = require('amqplib/callback_api');

let Worker = function(queueName) {
    amqp.connect('amqp://localhost', function(err, connection) {
        if (err) {
            console.log(err);
            return;
        }
        connection.createChannel(function(err, channel) {
            if (err) {
                console.log(err);
            }

            channel.assertQueue(queueName, {
                durable: false
            });

            console.log(" [*] Waiting for messages in %s", queueName);

            channel.consume(queueName, function(msg) {
                console.log(" [x] Received %s", msg.content.toString());
            }, {
                noAck: false
            });
        });
    });
}

module.exports = Worker;
