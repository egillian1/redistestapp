var express = require('express');
var router = express.Router();

var redis = require('redis');
var client = redis.createClient();

client.on('connect', function() {
    console.log('Redis connected in index.js');
});

client.on('error', function(err) {
    console.log('Redis error: ' + err);
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

module.exports = router;
