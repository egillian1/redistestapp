var express = require('express');
var router = express.Router();

var redis = require('redis');
var client = redis.createClient();

var amqp = require('amqplib/callback_api');
let channel;
let exchange = 'voteLogs';
amqp.connect('amqp://localhost', function(err, connection) {
    if(err) {
        console.log('RabbitMQ error:');
        console.log(err);
    }
    connection.createChannel(function(err, newChannel) {
        if(err) {
            console.log(err);
        }

        newChannel.assertExchange(exchange, 'fanout', {
            durable: false
        });

        newChannel.publish(exchange, '', Buffer.from('Test message'));

        channel = newChannel;

        console.log('Test message sent');
    });
});

client.on('connect', function() {
    console.log('Redis connected in vote.js');
    client.hset('votes','first', 0);
    client.hset('votes', 'second', 0);
    client.hset('votes', 'third', 0);
    client.hset('votes', 'fourth', 0);
});

client.on('error', function(err) {
    console.log('Redis error: ' + err);
});

var CronJob = require('cron').CronJob;
const job = new CronJob('*/10 * * * * *', () => {
    client.hgetall('votes', (err, results) => {
        client.HMSET('cachedResults', results);
    });
});
job.start();

router.post('/', function(req, res, next) {
    channel.publish(exchange, '', Buffer.from('Vote received for ' + req.body.vote));

    client.hincrby('votes', req.body.vote, 1, (err, results) => {
        return res.redirect('/vote/results');
    });
});

router.get('/results', function(req, res, next) {
    client.hgetall('cachedResults', (err, results) => {
        return res.render('results', results);
    });
});

module.exports = router;
