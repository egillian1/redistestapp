var amqp = require('amqplib/callback_api');
const fs = require('fs');

let ExchangeListener = function(exchange) {
    let handles = {};
    amqp.connect('amqp://localhost', function(err, connection) {
      if (err) {
          console.log(err);
          return;
      }
      connection.createChannel(function(err, channel) {
        if (err) {
            console.log(err);
            return;
        }

        channel.assertExchange(exchange, 'fanout', {
          durable: false
        });

        handles.channel = channel;
        handles.connection = connection;

        channel.assertQueue('exchangeLogger', {
          exclusive: false
        }, function(err, q) {
          if (err) {
              console.log(err);
          }
          console.log(" [*] Waiting for messages in %s", q.queue);
          channel.bindQueue(q.queue, exchange, '');

          channel.consume(q.queue, function(msg) {
            if(msg.content) {
                console.log(" [x] %s", msg.content.toString());
                fs.appendFile('vote.log', msg.content.toString() + '\n', function(err) {
                    if(err) {
                        console.log(err);
                    }
                });
            }
          }, {
            noAck: false
          });
        });
      });
    });
    return handles;
}

module.exports = ExchangeListener;
