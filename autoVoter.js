// Test script to send votes at set intervals
// Run in separate terminal using `node autoVoter.js`

var axios = require('axios');
var interval = 10;
var voteOptions = [
    "first",
    "second",
    "third",
    "fourth"
];

var randomOption = function() {
    var index = Math.floor(Math.random() * voteOptions.length);
    return voteOptions[index];
}

var randomVote = function() {
    axios.post('http://localhost:3000/vote/', {
        vote: randomOption()
    })
    .then((response) => {
        console.log('successful post of vote');
    })
    .catch((err) => {
        console.log(err);
    });
}

setInterval(function() {
    randomVote();
}, interval);
