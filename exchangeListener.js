var amqp = require('amqplib/callback_api');

let minTaskTime = 5;
let maxTaskTime = 300;

let getRandomTime = function() {
    return Math.floor(Math.random() * (minTaskTime - maxTaskTime) + minTaskTime);
}

let ExchangeListener = function(exchange) {
    let handles = {};
    amqp.connect('amqp://localhost', function(err, connection) {
      if (err) {
          console.log(err);
          return;
      }
      connection.createChannel(function(err, channel) {
        if (err) {
            console.log(err);
            return;
        }

        channel.assertExchange(exchange, 'fanout', {
          durable: false
        });

        // Only give 1 task to each worker at a time. This
        // prevents improper load balancing between workers.
        channel.prefetch(1);

        handles.channel = channel;
        handles.connection = connection;

        channel.assertQueue('exchangeListener', {
          exclusive: false
        }, function(err, q) {
          if (err) {
              console.log(err);
          }
          console.log(" [*] Waiting for messages in %s", q.queue);
          channel.bindQueue(q.queue, exchange, '');

          channel.consume(q.queue, function(msg) {
              setTimeout(function() {
                  if(msg.content) {
                      console.log(" [x] %s", msg.content.toString());
                      channel.ack(msg);
                  }
              }, getRandomTime());
          }, {noAck: false});
        });
      });
    });
    return handles;
}

module.exports = ExchangeListener;
